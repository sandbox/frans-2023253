<?php

$plugin = array(
  'fullname' => 'Twitter',
  'form alter' => 'twitter_form_alter',
);

function twitter_form_alter(&$form, &$form_state) {
  $form['config']['scope']['#access'] = FALSE;
}
