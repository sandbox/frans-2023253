<?php

$plugin = array(
  'fullname' => 'Google',
  'form alter' => 'google_form_alter',
);

function google_form_alter(&$form, &$form_state) {
  $form['config']['consumer_key']['#title'] = t('Client ID');
  $form['config']['consumer_secret']['#title'] = t('Client Secret');
  if (!isset($form['config']['scope']['#default_value']) || empty($form['config']['scope']['#default_value'])) {
    $form['config']['scope']['#default_value'] = 'profile';
  }
  $instance = isset($form_state['build_info']['args'][0])?$form_state['build_info']['args'][0]:NULL;
  $form['config']['offline_access'] = array(
    '#type' => 'checkbox',
    '#title' => t('Request offline access'),
    '#default_value' => (bool)(is_object($instance) && isset($instance->config) && is_array($instance->config) && isset($instance->config['offline_access']) && $instance->config['offline_access'])
  );
}