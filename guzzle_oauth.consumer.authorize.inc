<?php


function guzzle_oauth_consumer_authorize_page($consumer_instance, $action_name = 'default') {

  // Disable page cache.
  drupal_page_is_cacheable(FALSE);

  // normalize input action_name
  $action_name = preg_replace('/[^a-z0-9-]/i', '_', $action_name);
  if (empty($action_name)) {
    $action_name = 'default';
  }

  // Load a specific action
  $possible_actions = array();
  if (!empty($consumer_instance) && is_object($consumer_instance) && isset($consumer_instance->consumer)) {
    $possible_actions[] = $consumer_instance->consumer . '_' . $action_name;
  }
  $possible_actions[] = $action_name;
  if (!in_array('default', $possible_actions)) {
    $possible_actions[] = 'default';
  }
  do {
    $action = guzzle_oauth_action_load(array_shift($possible_actions));
  }
  while(empty($action) && count($possible_actions));

  // Access denied when no action is loaded here.
  if (empty($action) || !call_user_func_array($action['access callback'], $action['access arguments'])) {
    drupal_access_denied();
  }

  // execute pre authorize callback
  if (!empty($action['pre authorize callback']) && is_callable($action['pre authorize callback'])) {
    $action['pre authorize callback']($consumer_instance, $action);
  }

  // Bail out; no consumer instance found, although the Action could have loaded one.
  if (empty($consumer_instance)) {
    drupal_access_denied();
  }

  // Setup the client for this consumer instance.
  try {
    $client = guzzle_oauth_get_client_by_instance($consumer_instance);
  } catch (Exception $e) {
    drupal_access_denied();
  }

  // execute client alter
  if (!empty($action['client alter callback']) && is_callable($action['client alter callback'])) {
    $action['client alter callback']($client, $action);
  }

  // Get callback uri
  $callback_uri = url('go/callback/' . $consumer_instance->id, array('absolute' => TRUE));

  // Random string that can make a roundtrip
  $state = _guzzle_oauth_encrypt($consumer_instance->id . ':' . ip_address() . ':' . mt_rand());

  // Get request token.
  $request_token = NULL;
  try {
    $request_token = $client->getRequestToken($callback_uri);
  } catch(Exception $e) {};

  // Token to session
  $_SESSION['GO_' . $consumer_instance->id . '_REQUEST_TOKEN'] = serialize($request_token);

  // redirect the user
  try {
    $url = $client->getAuthorizeUrl($request_token, $callback_uri, $state);
  } catch(Exception $e) {};

  // execute post authorize callback
  if (!empty($action['post authorize callback']) && is_callable($action['post authorize callback'])) {
    $override_url = call_user_func($action['post authorize callback'], $request_token, $client, $url, $consumer_instance, $action);
  }

  // Action to session
  $_SESSION['GO_' . $consumer_instance->id . '_ACTION'] = $action['name'];
  if (isset($override_url) && !empty($override_url)) {
    $url = $override_url;
  }

  drupal_goto($url);
}
