<?php

$plugin = array(
  'fullname' => 'Facebook',
  'form alter' => 'facebook_form_alter',
);

function facebook_form_alter(&$form, &$form_state) {
  $form['config']['consumer_key']['#title'] = t('App ID');
  $form['config']['consumer_secret']['#title'] = t('App Secret');
  $instance = isset($form_state['build_info']['args'][0])?$form_state['build_info']['args'][0]:NULL;
  $form['config']['exchange_short_access_token'] = array(
    '#type' => 'checkbox',
    '#title' => t('Exchange short-lived access token for a long-lived access token'),
    '#default_value' => (bool)(is_object($instance) && isset($instance->config) && is_array($instance->config) && isset($instance->config['exchange_short_access_token']) && $instance->config['exchange_short_access_token'])
  );
}