<?php

function guzzle_oauth_consumers_overview() {

  $header = array(
    array(
      'data' => t('Title'),
      'field' => 'c.title',
    ),
    array(
      'data' => t('Consumer'),
      'field' => 'c.consumer',
    ),
    array(
      'data' => t('Status'),
      'field' => 'c.status',
    ),
    array(
      'data' => t('Callback URL'),
    ),
    array(
      'data' => t('Operations'),
    ),
  );

  $query = db_select('guzzle_oauth_consumer_instance', 'c')->extend('TableSort');
  $query->fields('c', array('title', 'consumer', 'status', 'id'));
  $query = $query->extend('PagerDefault')->limit(25);
  $result = $query->orderByHeader($header)->execute();

  $rows = array();
  foreach ($result as $row) {
    $row = (array) $row;

    $id = $row['id'];
    unset($row['id']);

    // Set callback url
    $row['callback_url'] = url('go/callback/' . $id, array('absolute' => TRUE));

    // Add operation links.
    $operation_links = array(
      array(
        'title' => t('Edit'),
        'href' => 'admin/config/services/guzzle-oauth-consumers/edit/' . $id,
        'attributes' => array(
          'title' => t('Edit'),
        ),
      ),
      array(
        'title' => t('Delete'),
        'href' => 'admin/config/services/guzzle-oauth-consumers/delete/' . $id,
        'attributes' => array(
          'title' => t('Delete'),
        ),
      ),
    );
    if ($row['status']) {
      $operation_links[] = array(
        'title' => t('Disable'),
        'href' => 'admin/config/services/guzzle-oauth-consumers/status/' . $id,
        'attributes' => array(
          'title' => t('Disable'),
        ),
      );
      $operation_links[] = array(
        'title' => t('Test'),
        'href' => 'admin/config/services/guzzle-oauth-consumers/test/' . $id,
        'attributes' => array(
          'title' => t('Test'),
        ),
      );
    }
    else {
      $operation_links[] = array(
        'title' => t('Enable'),
        'href' => 'admin/config/services/guzzle-oauth-consumers/status/' . $id,
        'attributes' => array(
          'title' => t('Enable'),
        ),
      );
    }

    $row['operations'] = array('data' => array(
      '#theme' => 'links__ctools_dropbutton',
      '#image' => FALSE,
      '#links' => $operation_links,
    ));

    // Human readable status
    $row['status'] = $row['status']?t('On'):t('Off');
    $rows[] = array('data' => $row);
  }

  // build the table for the nice output.
  $build['consumers_instances'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No Oauth consumers found. !link.', array('!link' => l(t('Add Oauth Consumer'), 'admin/config/services/guzzle-oauth-consumers/add'))),
  );
  $build['pager'] = array(
    '#theme' => 'pager',
  );

  return $build;

}

function guzzle_oauth_consumers_edit($form, &$form_state, $consumer_instance = NULL) {

  if (!isset($form_state['page'])) {
    $form_state['page'] = 0;
    $form_state['storage'] = array(
      'consumer_instance' => $consumer_instance,
    );
    if (!empty($consumer_instance) && isset($consumer_instance->id)) {
      $form_state['storage']['consumer'] = $consumer_instance->consumer;
      $form_state['page'] = 1;
    }
  }

  $page = $form_state['page'];
  $instance = $form_state['storage']['consumer_instance'];

  ctools_include('plugins');
  $consumers = ctools_get_plugins('guzzle_oauth', 'guzzle_oauth_consumers');

  $options = array();
  foreach($consumers as $consumer) {
    $options[$consumer['name']] = $consumer['fullname'];
  }


  if ($page == 0) {

    $form['consumer'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => $options,
      '#default_value' => isset($form_state['storage']['consumer'])?$form_state['storage']['consumer']:($instance?$instance->consumer:NULL),
    );

    $form['actions'] = array('#type' => 'actions');

    $form['actions']['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next'),
      '#submit' => array('guzzle_oauth_consumers_edit_next'),
    );
  } else {
    $consumer = isset($form_state['storage']['consumer'])?$form_state['storage']['consumer']:($instance->consumer?$instance->consumer:NULL);

    $form['consumer'] = array(
      '#type' => 'markup',
      '#markup' => '<b>' . t('Type') . '</b>: ' . check_plain($consumers[$consumer]['fullname']),
    );

    $form['status'] = array(
      '#type' => 'checkbox',
      '#title' => t('Active'),
      '#default_value' => array_key_exists('status', $form_state['storage']) ? $form_state['storage']['status'] :
          ($instance ? $instance->status :
          TRUE
        ),
    );

    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#description' => t('Human readable name, used for buttons and links.'),
      '#default_value' => isset($form_state['storage']['title']) ? $form_state['storage']['title'] :
          ($instance ? $instance->title :
          (isset($consumers[$consumer]['fullname']) ? $consumers[$consumer]['fullname'] :
          NULL
        )),
      '#required' => TRUE,
    );

    $form['config'] = array('#tree' => TRUE);
    if (!isset($form_state['storage']['config'])) {
      $form_state['storage']['config'] = array();
    }
    $form['config']['consumer_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Consumer key'),
      '#default_value' => isset($form_state['storage']['config']['consumer_key']) ? $form_state['storage']['config']['consumer_key'] :
          ($instance ? $instance->config['consumer_key'] :
          ''
        ),
      '#required' => TRUE,
    );

    $form['config']['consumer_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Consumer secret'),
      '#default_value' => isset($form_state['storage']['config']['consumer_secret']) ? $form_state['storage']['config']['consumer_secret'] :
          '',
      '#required' => TRUE,
    );

    $form['config']['scope'] = array(
      '#type' => 'textarea',
      '#title' => t('Scope'),
      '#default_value' => isset($form_state['storage']['config']['scope']) ? $form_state['storage']['config']['scope'] :
          ($instance ? $instance->config['scope'] :
          ''
        ),
      '#required' => FALSE,
    );

    // Let the plugin alter the form.
    if (isset($consumers[$consumer]['form alter']) && $consumers[$consumer]['form alter']) {
      $consumers[$consumer]['form alter']($form, $form_state);
    }

    // add missing descriptions.
    foreach (array('consumer_key', 'consumer_secret', 'scope') as $field) {
      if (!isset($form['config'][$field]['#description'])) {
        $form['config'][$field]['#description'] = t('@field_name provided by @consumer.', array(
          '@field_name' => $form['config'][$field]['#title'],
          '@consumer' => $consumers[$consumer]['fullname'],
        ));
      }
    }

    // Add instructions for existing consumers
    if ($instance && strlen($instance->config['consumer_secret'])) {
      $form['config']['consumer_secret']['#description'] .= ' <b>' . t('Leave empty to keep the current @field_name.', array(
        '@field_name' => $form['config']['consumer_secret']['#title']
      )) . '</b>';
      $form['config']['consumer_secret']['#required'] = FALSE;
    }


    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    $form['actions']['previous'] = array(
      '#type' => 'submit',
      '#value' => t('Change type'),
      '#submit' => array('guzzle_oauth_consumers_edit_previous'),
      '#limit_validation_errors' => array(),
    );
  }
  return $form;
}

/**
 * Cleanout storage value.
 */
function _guzzle_oauth_consumers_edit_clean_storage(&$form_state) {
  foreach(array('form_id', 'form_token', 'form_build_id', 'submit', 'previous', 'next', 'op') as $key) {
    if(isset($form_state['storage'][$key])) {
      unset($form_state['storage'][$key]);
    }
  }
}

/**
 * Next step.
 */
function guzzle_oauth_consumers_edit_next(&$form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $form_state['page']++;
  $form_state['storage'] = array_merge($form_state['storage'], $form_state['values']);
  _guzzle_oauth_consumers_edit_clean_storage($form_state);
}

/**
 * Previous step.
 */
function guzzle_oauth_consumers_edit_previous(&$form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $form_state['page']--;
  $form_state['storage'] = array_merge($form_state['storage'], $form_state['input']);
  ctools_include('plugins');
  $consumer = ctools_get_plugins('guzzle_oauth', 'guzzle_oauth_consumers', $form_state['storage']['consumer']);
  // Remove title if title is not changed.
  if ($consumer['fullname'] == $form_state['storage']['title']) {
    unset($form_state['storage']['title']);
  }
  _guzzle_oauth_consumers_edit_clean_storage($form_state);
}

/**
 * Edit/new submit callback.
 */
function guzzle_oauth_consumers_edit_submit(&$form, &$form_state) {
  $form_state['storage'] = array_merge($form_state['storage'], $form_state['values']);
  _guzzle_oauth_consumers_edit_clean_storage($form_state);

  $instance = new stdClass;
  $instance->id = $form_state['storage']['consumer_instance']?$form_state['storage']['consumer_instance']->id:NULL;
  $instance->consumer = $form_state['storage']['consumer'];
  $instance->title = $form_state['storage']['title'];
  $instance->status = $form_state['storage']['status'];
  $instance->config = $form_state['storage']['config'];
  if (!strlen($instance->config['consumer_secret']) && $form_state['storage']['consumer_instance']->config['consumer_secret']) {
    $instance->config['consumer_secret'] = $form_state['storage']['consumer_instance']->config['consumer_secret'];
  }
  guzzle_oauth_consumer_instance_save($instance);
  $form_state['redirect'] = 'admin/config/services/guzzle-oauth-consumers';
  drupal_set_message(t('Oauth Consumer @title saved.', array('@title' => $instance->title)));
}

/**
 * Flip status.
 */
function guzzle_oauth_consumers_status_switch($instance) {
  $instance->status = !$instance->status;
  guzzle_oauth_consumer_instance_save($instance);
  drupal_set_message(t('Oauth Consumer @title is now @status.', array('@title' => $instance->title, '@status' => $instance->status?t('on'):t('off'))));
  drupal_goto('admin/config/services/guzzle-oauth-consumers');
}

/**
 * Menu callback for a delete.
 */
function guzzle_oauth_consumers_delete($form, &$form_state, $consumer_instance) {
  $form['#instance'] = $consumer_instance;
  return confirm_form($form,
    t('Are you sure you want to delete Oauth Consumer %title?', array('%title' => $consumer_instance->title)),
    'admin/config/services/guzzle-oauth-consumers',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Confirm delete.
 */
function guzzle_oauth_consumers_delete_submit($form, &$form_state) {
  if($form_state['values']['confirm']) {
    guzzle_oauth_consumer_instance_delete($form['#instance']);
    watchdog('guzzle_oauth', '@type: deleted %title.', array('@type' => $form['#instance']->consumer, '%title' => $form['#instance']->title));
    drupal_set_message(t('%title (@type) has been deleted.', array('@type' => $form['#instance']->consumer, '%title' => $form['#instance']->title)));
  };
  $form_state['redirect'] = 'admin/config/services/guzzle-oauth-consumers';
}

/**
 * Test a oauth consumer.
 *
 * @TODO: finish this.
 */
function guzzle_oauth_consumers_test($consumer_instance) {
  $display = array();
  $display['config'] = array(
    '#type' => 'markup',
    '#markup' => '<pre>' . print_r($consumer_instance->config, TRUE) . '</pre>',
  );
  return $display;
}
