<?php

function guzzle_oauth_consumer_redirect_page($consumer_instance) {

  // Disable page cache.
  drupal_page_is_cacheable(FALSE);

  // Get action name.
  $action_name = 'default';
  if (isset($_SESSION['GO_' . $consumer_instance->id . '_ACTION'])) {
    $action_name = $_SESSION['GO_' . $consumer_instance->id . '_ACTION'];
  }

  // Load a specific action
  $possible_actions = array($action_name, 'default');
  do {
    $action = guzzle_oauth_action_load(array_shift($possible_actions));
  }
  while(empty($action) && count($possible_actions));

  // Access denied when no action is loaded here.
  if (empty($action) || !call_user_func_array($action['access callback'], $action['access arguments'])) {
    drupal_access_denied();
  }

  // execute pre callback callback
  if (!empty($action['pre callback callback']) && is_callable($action['pre callback callback'])) {
    $action['pre callback callback']($consumer_instance, $action);
  }

  // Setup the client for this consumer instance.
  try {
    $client = guzzle_oauth_get_client_by_instance($consumer_instance);
  } catch (Exception $e) {
    drupal_access_denied();
  }

  // execute client alter
  if (!empty($action['client alter callback']) && is_callable($action['client alter callback'])) {
    $action['client alter callback']($client, $action);
  }

  if (!empty($_GET) && isset($_SESSION['GO_' . $consumer_instance->id . '_REQUEST_TOKEN'])) {

    // Check state if we have one.
    if (isset($_GET['state'])) {
      $state = explode(':', _guzzle_oauth_decrypt(str_replace(' ', '+', $_GET['state'])));
      if ($state[0] != $consumer_instance->id || $state[1] != ip_address()) {
        drupal_access_denied();
      }
    }

    $request_token = unserialize($_SESSION['GO_' . $consumer_instance->id . '_REQUEST_TOKEN']);
    $access_token = NULL;

    try {
      $access_token = $client->getAccessToken($_GET, $request_token);
    } catch(exception $e){};

    $_SESSION['GO_' . $consumer_instance->id . '_ACCESS_TOKEN'] = serialize($access_token);

    unset($_SESSION['GO_' . $consumer_instance->id . '_REQUEST_TOKEN']);
    unset($_SESSION['GO_' . $consumer_instance->id . '_ACTION']);

    // execute post callback callback
    if (!empty($action['post callback callback']) && is_callable($action['post callback callback'])) {
      return call_user_func($action['post callback callback'], $access_token, $client, $request_token, $consumer_instance, $action);
    }

  }
  drupal_not_found();
}
