<?php


/**
 * Implements HOOK_menu().
 */
function guzzle_oauth_menu() {
  $items = array();
  $items['go/callback/%guzzle_oauth_consumer_instance'] = array(
    'title' => 'Guzzle Oauth Callback',
    'page callback' => 'guzzle_oauth_consumer_redirect_page',
    'page arguments' => array(2),
    'access arguments' => array('access content'),
    'file' => 'guzzle_oauth.consumer.callback.inc',
    'type' => MENU_CALLBACK,
  );
  $items['go/authorize/%guzzle_oauth_consumer_instance'] = array(
    'title' => 'Guzzle Oauth Authorize',
    'page callback' => 'guzzle_oauth_consumer_authorize_page',
    'page arguments' => array(2, 3),
    'access arguments' => array('access content'),
    'file' => 'guzzle_oauth.consumer.authorize.inc',
    'type' => MENU_CALLBACK,
  );
  $items['go/%'] = array(
    'title' => 'Guzzle Oauth Authorize',
    'page callback' => 'guzzle_oauth_consumer_authorize_page',
    'page arguments' => array(NULL, 1),
    'access arguments' => array('access content'),
    'file' => 'guzzle_oauth.consumer.authorize.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/config/services/guzzle-oauth-consumers'] = array(
    'title' => 'Oauth Consumers',
    'description' => 'Configure the Oauth Consumers.',
    'page callback' => 'guzzle_oauth_consumers_overview',
    'access arguments' => array('administer site configuration'),
    'file' => 'guzzle_oauth.admin.inc',
  );
  $items['admin/config/services/guzzle-oauth-consumers/overview'] = array(
    'title' => 'Oauth Consumers',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -1,
  );
  $items['admin/config/services/guzzle-oauth-consumers/add'] = array(
    'title' => 'Add Oauth Consumer',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guzzle_oauth_consumers_edit'),
    'access arguments' => array('administer site configuration'),
    'file' => 'guzzle_oauth.admin.inc',
    'type' => MENU_LOCAL_ACTION,
  );
  $items['admin/config/services/guzzle-oauth-consumers/edit/%guzzle_oauth_consumer_instance'] = array(
    'title' => 'Edit Oauth Consumer',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guzzle_oauth_consumers_edit', 5),
    'access arguments' => array('administer site configuration'),
    'file' => 'guzzle_oauth.admin.inc',
    'type' => MENU_LOCAL_ACTION,
  );
  $items['admin/config/services/guzzle-oauth-consumers/delete/%guzzle_oauth_consumer_instance'] = array(
    'title' => 'Delete Oauth Consumer',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guzzle_oauth_consumers_delete', 5),
    'access arguments' => array('administer site configuration'),
    'file' => 'guzzle_oauth.admin.inc',
    'type' => MENU_LOCAL_ACTION,
  );
  $items['admin/config/services/guzzle-oauth-consumers/status/%guzzle_oauth_consumer_instance'] = array(
    'title' => 'Switch Status Oauth Consumer',
    'page callback' => 'guzzle_oauth_consumers_status_switch',
    'page arguments' => array(5),
    'access arguments' => array('administer site configuration'),
    'file' => 'guzzle_oauth.admin.inc',
    'type' => MENU_LOCAL_ACTION,
  );
  $items['admin/config/services/guzzle-oauth-consumers/test/%guzzle_oauth_consumer_instance'] = array(
    'title' => 'Test Oauth Consumer',
    'page callback' => 'guzzle_oauth_consumers_test',
    'page arguments' => array(5),
    'access arguments' => array('administer site configuration'),
    'file' => 'guzzle_oauth.admin.inc',
    'type' => MENU_LOCAL_ACTION,
  );

  return $items;
}

/**
 * Load a client class via ctools plugins.
 */
function &guzzle_oauth_get_client($consumer_name, $config = array()) {
  ctools_include('plugins');
  $consumer_config = ctools_get_plugins('guzzle_oauth', 'guzzle_oauth_consumers', $consumer_name);
  if (!call_user_func_array($consumer_config['access callback'], $consumer_config['access arguments'])) {
    throw new Exception('No access to this consumer.', 1);
  }
  return $consumer_config['load consumer class']($consumer_config, $config);
}

/**
 * Load a client class via ctools plugins by instance.
 */
function &guzzle_oauth_get_client_by_instance($consumer_instance, $config = array()) {
  if (!is_object($consumer_instance)) {
    $consumer_instance = guzzle_oauth_consumer_instance_load($consumer_instance);
  }
  if (!$consumer_instance || !isset($consumer_instance->config) || !is_array($consumer_instance->config)) {
    throw new Exception('Could not load consumer instance.', 1);
  }
  $config = array_merge($consumer_instance->config, $config);
  return guzzle_oauth_get_client($consumer_instance->consumer, $config);
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function guzzle_oauth_ctools_plugin_directory($module, $type) {
  if ($module == 'guzzle_oauth' && $type =='guzzle_oauth_actions') {
    return 'plugins/guzzle_oauth_actions';
  }
  elseif ($module == 'guzzle_oauth' && $type =='guzzle_oauth_consumers') {
    return 'plugins/guzzle_oauth_consumers';
  }
}

/**
 * Implements hook_ctools_plugin_type()
 *
 * Inform CTools about guzzle_oauth_consumers and guzzle_oauth_actions.
 */
function guzzle_oauth_ctools_plugin_type() {
  $plugins = array();
  $plugins['guzzle_oauth_consumers'] = array(
    'defaults' => array(
      'fullname' => '',
      'form alter' => NULL,
      'load consumer class' => 'guzzle_oauth_default_load_consumer_class',
      'access callback' => 'user_access',
      'access arguments' => array('access content'),
    ),
  );
  $plugins['guzzle_oauth_actions'] = array(
    'defaults' => array(
      'consumers' => array(),
      'client alter callback' => NULL,
      'pre authorize callback' => NULL,
      'post authorize callback' => 'guzzle_oauth_default_post_authorize_callback',
      'pre callback callback' => NULL,
      'post callback callback' => 'guzzle_oauth_default_post_callback_callback',
      'access callback' => 'user_access',
      'access arguments' => array('access content'),
    ),
  );
  return $plugins;
}

/**
 * Default consumer loader.
 */
function &guzzle_oauth_default_load_consumer_class($consumer_config, $instance_config) {
  $consumer = \GuzzleOauth\Consumers::get(strtolower($consumer_config['name']), $instance_config);
  return $consumer;
}

/**
 * Default post authorize callback, stores destination in session.
 * NB. request_token and action['name'] is already in SESSION storage.
 * $_SESSION['GO_' . ($consumer_instance->id) . '_REQUEST_TOKEN']
 * $_SESSION['GO_' . ($consumer_instance->id) . '_ACTION']
 *
 * $request_token
 *    values from request token endpoint
 * $client
 *    Client Class
 * $url
 *    URL user will be send to after this callback
 * $action
 *    guzzle_oauth_action array
 */
function guzzle_oauth_default_post_authorize_callback($request_token, $client, $url, $consumer_instance, $action) {
  $destination = drupal_get_destination();
  if ($destination['destination'] == $_GET['q'] || drupal_substr($destination['destination'], 0, strpos($destination['destination'], '?')) == $_GET['q']) {
    $destination = NULL;
  }
  $_SESSION['GO_' . $consumer_instance->id . '_DESTINATION'] = serialize($destination);

  //Reset destination.
  unset($_GET['destination']);
  drupal_static_reset('drupal_get_destination');
  return $url;
}


/**
 * Default post callback callback, redirect user to destination.
 * NB. access_token is already in SESSION storage.
 * $_SESSION['GO_' . $consumer_config->id . '_ACCESS_TOKEN']
 * request_token and action['name'] are removed from SESSION storage.
 * $_SESSION['GO_' . $consumer_config->id . '_REQUEST_TOKEN']
 * $_SESSION['GO_' . $consumer_config->id . '_ACTION']
 *
 * $access_token
 *    Access Tokken Array
 * $client
 *    client Class
 * $request_token
 *    values from request token endpoint
 * $action
 *    guzzle_oauth_action array
 */
function guzzle_oauth_default_post_callback_callback($access_token, $client, $request_token, $consumer_instance, $action) {
  $destination = unserialize($_SESSION['GO_' . $consumer_instance->id . '_DESTINATION']);
  unset($_SESSION['GO_' . $consumer_instance->id . '_DESTINATION']);
  $options = array();
  $path = NULL;
  if (isset($destination['destination']) && !url_is_external($destination['destination'])) {
    $url_parts = drupal_parse_url($destination['destination']);
    $path = $url_parts['path'];
    if (!empty($url_parts['query'])) {
      $options['query'] = $url_parts['query'];
    }
    if (!empty($url_parts['fragment'])) {
      $options['fragment'] = $url_parts['fragment'];
    }
  }
  //Reset destination.
  unset($_GET['destination']);
  drupal_static_reset('drupal_get_destination');
  drupal_goto($path, $options);
}

/**
 * Load a consumer_instance.
 */
function guzzle_oauth_consumer_instance_load($id) {
  $instance = db_select('guzzle_oauth_consumer_instance', 'c')
    ->fields('c')
    ->condition('id', $id)
    ->execute()
    ->fetch();
  if (isset($instance->config)) {
    $instance->config = unserialize($instance->config);
    if(isset($instance->config['consumer_secret'])) {
      $instance->config['consumer_secret'] = _guzzle_oauth_decrypt($instance->config['consumer_secret']);
    }
  }
  return $instance;
}

/**
 * Save a consumer instance.
 */
function guzzle_oauth_consumer_instance_save($instance) {
  $record = (array)$instance;
  $pk = array();
  if (isset($record['id']) && !empty($record['id'])) {
    $pk[] = 'id';
  }
  if (isset($record['config']) && isset($record['config']['consumer_secret'])) {
    $record['config']['consumer_secret'] = _guzzle_oauth_encrypt($record['config']['consumer_secret']);
  }
  drupal_write_record('guzzle_oauth_consumer_instance', $record, $pk);
  return $instance;
}

/**
 * Delete a consumer instance.
 */
function guzzle_oauth_consumer_instance_delete($id) {
  if (is_object($id)) {
    $id = $id->id;
  }
  db_delete('guzzle_oauth_consumer_instance')
    ->condition('id', $id)
    ->execute();
  return TRUE;
}

/**
 * Implements 'load callback' for guzzle_oauth_actions.
 * Defined for menu callbacks.
 */
function guzzle_oauth_action_load($action_name = NULL) {
  if (empty($action_name)) {
    $action_name = 'default';
  }
  ctools_include('plugins');
  return ctools_get_plugins('guzzle_oauth', 'guzzle_oauth_actions', $action_name);
}

/**
 * Helper function to encrypt stored passwords, secrets and access_tokens.
 */
function _guzzle_oauth_encrypt($str) {
  $key   = md5(drupal_get_hash_salt());
  $iv    = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND);
  return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $str, MCRYPT_MODE_ECB, $iv)));
}

/**
 * Helper function to decrypt stored passwords, secrets and access_tokens.
 */
function _guzzle_oauth_decrypt($str) {
  $ivsize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
  $iv     = mcrypt_create_iv($ivsize, MCRYPT_RAND);
  $key    = md5(drupal_get_hash_salt());
  return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode($str), MCRYPT_MODE_ECB, $iv));
}


