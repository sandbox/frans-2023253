<?php

$plugin = array(
  'fullname' => 'LinkedIn',
  'form alter' => 'linkedin_form_alter',
);

function linkedin_form_alter(&$form, &$form_state) {
  $form['config']['consumer_key']['#title'] = t('API Key');
  $form['config']['consumer_secret']['#title'] = t('Secret Key');
  if (!isset($form['config']['scope']['#default_value']) || empty($form['config']['scope']['#default_value'])) {
    $form['config']['scope']['#default_value'] = 'r_basicprofile';
  }
}