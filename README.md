Guzzle oAuth provides a library and framework for oAuth based REST api's.
It is based on Guzzle the default http client for Drupal 8.

The module by itself only lets you configure consumer instances in the backend.
It provides consumers for Facebook, Google, LinkedIn and Twitter. Developers
can use cTools Plugins to add their own consumers (or edit existing ones). See
the guzzle_oauth_meetup.module for an example of another provider then the
default four. We have tried to make the form fields as transparent as possible
by using the same naming in the forms in Drupal as used on the API pages of the
providers.

The default action on authorization is to store a access token in the current
session. Actions are defined with cTools Plugins, see the
guzzle_oauth_login_register.module for a custom action. But the default action
is often enough to do your thing. See for example the
guzzle_oauth_feeds.module.

Extra Modules
-------------
- **Guzzle oAuth Feeds**
    Can read and import from oAuth protected feeds (Google Docs).
- **Guzzle oAuth Accounts**
    Stores accounts and access tokens in the database and relate them (or not) to entities. For example relate a twitter account to a Drupal user.
- **Guzzle oAuth Login Register**
    Login or register via oAuth services.
- **Guzzle oAuth Meetup**
    Use meetup.com as a provider and use their REST api.

### Install
Download and install the module as usual. The module has a dependency on ctools
and on composer_autoload.
Now that the module is installed you need the libraries via composer or drush.
### Install libraries via composer
On the command-line go to the folder of the module. (The folder where also
composer.json is).
Download composer
```bash
curl -sS https://getcomposer.org/installer | php
```
Install the libraries
```bash
php composer.phar install
```
### Install libraries via drush
Be sure to have the
[composer](https://drupal.org/project/composer)
module installed in Drush.
On the command-line go to the folder of the module. (The folder where also
composer.json is).
Install the libraries
```bash
drush composer install
```
### Composer in Drupal
You might have expected a dependency on the module [guzzle](https://drupal.org/project/guzzle).
But composer in Drupal 7 is broken. See https://drupal.org/project/composer for all kinds of
solutions. Since the Guzzle module does nothing more then a composer.json that includes
guzzle/guzzle... we have decided to let the guzzle dependency go and have guzzle in the vendor
dir of this module. You are allowed to disagree... but then you are on your own; find your
preferred solution on the composer project page.
